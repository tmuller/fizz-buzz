class FizzBuzz {
  countUp(limit) {

    let output = [];
    for (let idx = 1; idx <= limit; idx++) {
      output.push(this.calculateOutput(idx));
    }
    return output;
  }

  calculateOutput(num) {
    let outputString = '';

    if (num % 3 === 0) outputString +=  'Fizz';
    if (num % 5 === 0) outputString +=  'Buzz';

    if (outputString.length < 1) return num.toString();
    else return outputString;
  }

}

module.exports = FizzBuzz;
