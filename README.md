# Coding exercise

Implement the [FizzBuzz](https://ccd-school.de/coding-dojo/function-katas/fizzbuzz/) 

# Jasmine on Node.js

This directory contains a basic project template using [_Node.js_](https://nodejs.org/) and [_Jasmine_](https://jasmine.github.io/).

To install dependencies:

```bash
npm install
```

To run the test suite:

```bash
npm test
```
