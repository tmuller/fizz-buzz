const FizzBuzz = require('../src/FizzBuzz');

describe('FizzBuzz', () => {
  describe('countUp()', () => {

    let classUnderTest;

    beforeEach(() => {
      classUnderTest = new FizzBuzz();
    })

    it('should return an array with 1 init is passed 1', () => {
      const result = classUnderTest.countUp(1);
      expect(result).toEqual(['1']);
    });

    it('should return an array with [1,2,Fizz] when passed an argument of 3', () => {
      const result = classUnderTest.countUp(3);
      expect(result).toEqual(['1', '2', 'Fizz']);
    });

    it('should return an array contains Buzz if counting to 5', () => {
      const result = classUnderTest.countUp(5);
      expect(result).toEqual(['1', '2', 'Fizz', '4', 'Buzz']);
    });

    it('should return "FizzBuzz" if number is divisble by 15', () => {

      const fizzbuzzValues = [15, 30, 45, 60];

      fizzbuzzValues.forEach( num => {
        const result = classUnderTest.countUp(num);
        expect(result[num - 1]).toEqual('FizzBuzz');
      })
    });

  });
});
